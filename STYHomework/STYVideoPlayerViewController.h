//
//  STYVideoPlayerViewController.h
//  STYHomework
//
//  Created by bytedance on 2021/7/14.
//

#import <UIKit/UIKit.h>
#import <Photos/Photos.h>
#import <AVFoundation/AVFoundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface STYVideoPlayerViewController : UIViewController
@property (nonatomic, strong) PHAsset *asset;
@end

NS_ASSUME_NONNULL_END
