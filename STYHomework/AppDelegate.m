//
//  AppDelegate.m
//  STYHomework
//
//  Created by bytedance on 2021/7/13.
//

#import "AppDelegate.h"
#import "STYAlbumCollectionViewController.h"
#import "STYNavigationController.h"
#import "STYRootCollectionViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
//    STYAlbumCollectionViewController *cvc = [[STYAlbumCollectionViewController alloc] init];
//    STYNavigationController *navigationController = [[STYNavigationController alloc] initWithRootViewController:cvc];
    
    STYRootCollectionViewController *rcvc = [[STYRootCollectionViewController alloc] init];
    STYNavigationController *navigationController = [[STYNavigationController alloc] initWithRootViewController:rcvc];
    
    self.window.rootViewController = navigationController;
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    return YES;
}
@end
