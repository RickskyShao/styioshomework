//
//  STYVideoPlayerViewController.m
//  STYHomework
//
//  Created by bytedance on 2021/7/14.
//

#import "STYVideoPlayerViewController.h"
#import "STYVideoPlayerView.h"

@interface STYVideoPlayerViewController ()
@property (nonatomic, strong) STYVideoPlayerView *playerView;
@end

@implementation STYVideoPlayerViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.playerView = [[STYVideoPlayerView alloc] init];
        _playerView.frame = [UIScreen mainScreen].bounds;
        [self.view addSubview:self.playerView];
    }
    return self;
}

- (void)setAsset:(PHAsset *)asset
{
    _asset = asset;
    PHVideoRequestOptions *option = [[PHVideoRequestOptions alloc] init];
    [[PHImageManager defaultManager] requestAVAssetForVideo:_asset options:option resultHandler:^(AVAsset * _Nullable asset, AVAudioMix * _Nullable audioMix, NSDictionary * _Nullable info) {
        dispatch_async(dispatch_get_main_queue(), ^{
            AVPlayerItem *playerItem = [AVPlayerItem playerItemWithAsset:asset];
              [self.playerView.player replaceCurrentItemWithPlayerItem:playerItem];
              [self.playerView.player play];
        });
    }];
}

@end
