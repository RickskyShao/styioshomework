//
//  STYCaptureViewController.m
//  STYHomework
//
//  Created by bytedance on 2021/7/15.
//

#import "STYCaptureViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <Photos/Photos.h>
#import "STYPhotoStore.h"

@interface STYCaptureViewController () <AVCapturePhotoCaptureDelegate, AVCaptureFileOutputRecordingDelegate>
@property (nonatomic, strong) AVCaptureSession *session;
@property (nonatomic, strong) AVCaptureDeviceInput *deviceVideoInput;
@property (nonatomic, strong) AVCaptureDeviceInput *deviceAudioInput;
@property (nonatomic, strong) AVCaptureMovieFileOutput *videoOuput;
@property (nonatomic, strong) AVCapturePhotoOutput *photoOutput;
@property (nonatomic, strong) AVCaptureConnection *connection;

@property (nonatomic, strong) AVCaptureVideoPreviewLayer *previewLayer;

@property (nonatomic, strong) UIButton *shutterButton;
@property (nonatomic, strong) UIToolbar *toolBar;
@property (nonatomic, strong) UISegmentedControl *segControl;
@property (nonatomic, strong) NSData *data;
@end

@implementation STYCaptureViewController

- (instancetype)init
{
    if (self = [super init]) {
        self.view.frame = [UIScreen mainScreen].bounds;
        [self setUpShutterButton];
        [self setUpSegControl];
    }
    return self;
}

- (void)setUpShutterButton
{
    _shutterButton = [UIButton buttonWithType:UIButtonTypeSystem];
    CGRect frame = [UIScreen mainScreen].bounds;
    _shutterButton.frame = CGRectMake(frame.size.width / 2 - 35, frame.size.height - 105, 70, 60);
    _shutterButton.backgroundColor = [UIColor whiteColor];
    
    UIImage* imageShutte = [UIImage systemImageNamed:@"camera"];
    [_shutterButton setBackgroundImage:imageShutte forState:UIControlStateNormal];
    UIImage* imageStop = [UIImage systemImageNamed:@"video.slash"];
    [_shutterButton setBackgroundImage:imageStop forState:UIControlStateSelected];

    _shutterButton.tintColor = [UIColor blackColor];
    [_shutterButton addTarget:self action:@selector(shutterCamera) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_shutterButton];
}

- (void)setUpSegControl
{
    NSArray *arr = [[NSArray alloc] initWithObjects:@"录像", @"拍照", @"Live Photo", nil];
    _segControl = [[UISegmentedControl alloc] initWithItems:arr];
    CGRect frame = [UIScreen mainScreen].bounds;
    _segControl.frame = CGRectMake(frame.size.width / 2 - 150, frame.size.height - 150, 300, 30);
    _segControl.backgroundColor = [UIColor clearColor];
    _segControl.selectedSegmentIndex = 1;
    [self.view addSubview:_segControl];
}

- (void)shutterCamera
{
    if (_segControl.selectedSegmentIndex == 1) {
        NSLog(@"选中为拍照状态啦！");
        [_session removeOutput:_videoOuput];
        _connection = [_photoOutput connectionWithMediaType:AVMediaTypeVideo];
        if (!_connection) {
            return;
        }
        AVCapturePhotoSettings *photoSetting = [AVCapturePhotoSettings photoSettings];
        [_photoOutput capturePhotoWithSettings:photoSetting delegate:self];
    } else if (_segControl.selectedSegmentIndex == 0) {
        NSLog(@"选中为录像状态啦！");
        // 录像是否在进行中
        if (![_videoOuput isRecording]) {
            [_session addOutput:_videoOuput];
            _connection.videoOrientation=[_previewLayer connection].videoOrientation;
            
            NSString *outputFielPath=[NSTemporaryDirectory() stringByAppendingString:@"myMovie.mov"];
            NSLog(@"save path is :%@",outputFielPath);
            NSURL *fileUrl=[NSURL fileURLWithPath:outputFielPath];
            
            [_videoOuput startRecordingToOutputFileURL:fileUrl recordingDelegate:self];
            [_shutterButton setSelected:YES];
        } else {
            [_videoOuput stopRecording];
            [_shutterButton setSelected:NO];
            [_session removeOutput:_videoOuput];
        }
    } else if (_segControl.selectedSegmentIndex == 2) {
        NSLog(@"选中为Live Photo状态啦！");
        _photoOutput.livePhotoCaptureEnabled = YES;
//        AVCapturePhotoSettings *photoSetting = [AVCapturePhotoSettings photoSettingsWithFormat:[NSDictionary dictionaryWithObject:AVVideoCodecTypeHEVC forKey:AVVideoCodecKey]];
        AVCapturePhotoSettings *photoSetting = [AVCapturePhotoSettings photoSettings];
        
        NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [path objectAtIndex:0];
        NSDateFormatter *formatter= [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"YYYY-MM-dd-HH-mm-ss"];
        NSString *fileName = [formatter stringFromDate:[NSDate date]];
        NSMutableString *fullURL = [NSMutableString stringWithString:documentsDirectory];
        [fullURL appendString:@"/"];
        [fullURL appendString:fileName];
        [fullURL appendString:@"-livephoto.mov"];
        NSURL *fileURL = [NSURL fileURLWithPath:fullURL];
        
        photoSetting.livePhotoMovieFileURL = fileURL;
        self.photoOutput.livePhotoCaptureEnabled = self.photoOutput.isLivePhotoCaptureSupported;
        [self.photoOutput capturePhotoWithSettings:photoSetting delegate:self];
    }
}
#pragma mark - 照片视频输出代理
// 照片代理
- (void)captureOutput:(AVCapturePhotoOutput *)output didFinishProcessingPhoto:(AVCapturePhoto *)photo error:(NSError *)error
{
    NSData *data = [photo fileDataRepresentation];
    if (_segControl.selectedSegmentIndex == 2) {
        _data = data;
        NSLog(@"live photo image load");
    } else {
        UIImage *image = [UIImage imageWithData:data];
        if (image) {
            [[PHPhotoLibrary sharedPhotoLibrary]performChanges:^{
                [PHAssetChangeRequest creationRequestForAssetFromImage:image];
            } completionHandler:^(BOOL success, NSError * _Nullable error) {
                if (error) {
                    NSLog(@"%@",@"照片保存失败");
                } else {
                    NSLog(@"%@",@"照片保存成功");
                }
            }];
        }
    }
}
// 视频代理
- (void)captureOutput:(AVCaptureFileOutput *)output didStartRecordingToOutputFileAtURL:(NSURL *)fileURL fromConnections:(NSArray<AVCaptureConnection *> *)connections
{
    NSLog(@"开始录制。。。。。。");
}
- (void)captureOutput:(AVCaptureFileOutput *)output didFinishRecordingToOutputFileAtURL:(NSURL *)outputFileURL fromConnections:(NSArray<AVCaptureConnection *> *)connections error:(NSError *)error
{
    NSLog(@"视频录制完成！");
    UISaveVideoAtPathToSavedPhotosAlbum([outputFileURL path], nil, nil, nil);
}
// live photo拍摄代理
- (void)captureOutput:(AVCapturePhotoOutput *)output didFinishProcessingLivePhotoToMovieFileAtURL:(NSURL *)outputFileURL duration:(CMTime)duration photoDisplayTime:(CMTime)photoDisplayTime resolvedSettings:(AVCaptureResolvedPhotoSettings *)resolvedSettings error:(NSError *)error
{
    NSLog(@"live photo保存...");

    [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
        PHAssetCreationRequest *request = [PHAssetCreationRequest creationRequestForAsset];
        [request addResourceWithType:PHAssetResourceTypePhoto data:self.data options:nil];
        PHAssetResourceCreationOptions *options = [[PHAssetResourceCreationOptions alloc]init];
        options.shouldMoveFile = YES;
        [request addResourceWithType:PHAssetResourceTypePairedVideo fileURL:outputFileURL options:options];
    } completionHandler:^(BOOL success, NSError * _Nullable error) {
        NSLog(@"Finished uploading LivePhoto. %@", (success ? @"Success." : error));
    }];
}
# pragma mark - View Life Cycle
- (void) viewDidLoad
{
    [super viewDidLoad];
    
    _session = [[AVCaptureSession alloc] init];
    [_session beginConfiguration];
    
    AVCaptureDevice *deviceVideo = [AVCaptureDevice defaultDeviceWithDeviceType:AVCaptureDeviceTypeBuiltInWideAngleCamera mediaType:AVMediaTypeVideo position:AVCaptureDevicePositionBack];
    _deviceVideoInput = [AVCaptureDeviceInput deviceInputWithDevice:deviceVideo error:nil];
    AVCaptureDevice *deviceAudio = [AVCaptureDevice defaultDeviceWithDeviceType:AVCaptureDeviceTypeBuiltInMicrophone mediaType:AVMediaTypeAudio position:AVCaptureDevicePositionUnspecified];
    _deviceAudioInput = [AVCaptureDeviceInput deviceInputWithDevice:deviceAudio error:nil];
    
    if ([_session canAddInput:_deviceVideoInput]) {
        [_session addInput:_deviceVideoInput];
    }
    if ([_session canAddInput:_deviceAudioInput]) {
        [_session addInput:_deviceAudioInput];
    }
    // 添加照片输出和视频输出
    _photoOutput = [[AVCapturePhotoOutput alloc] init];
    if ([_session canAddOutput:_photoOutput]) {
        [_session addOutput:_photoOutput];
    }
    _videoOuput = [[AVCaptureMovieFileOutput alloc] init];
    
    _connection = [_photoOutput connectionWithMediaType:AVMediaTypeVideo];
    if ([_connection isVideoOrientationSupported])
    {
        [_connection setVideoOrientation: AVCaptureVideoOrientationPortrait];
    }
    
    _session.sessionPreset = AVCaptureSessionPresetPhoto;
    [_session commitConfiguration];
    // 创建视频预览层，实时展示摄像头状态
    _previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_session];
    CALayer *rootLayer = self.view.layer;
    _previewLayer.frame = rootLayer.bounds;
    [rootLayer addSublayer:_previewLayer];
    
    [_session startRunning];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [_session stopRunning];
    
    [[STYPhotoStore sharedStore] loadAssets];
}
@end
