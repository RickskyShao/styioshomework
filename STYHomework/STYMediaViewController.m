//
//  STYMediaViewController.m
//  STYHomework
//
//  Created by bytedance on 2021/7/14.
//

#import "STYMediaViewController.h"
#import "STYMediaView.h"

@interface STYMediaViewController ()
@property (nonatomic) STYMediaView *mediaView;
@end

@implementation STYMediaViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.mediaView = [[STYMediaView alloc] init];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.mediaView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //    PHImageRequestOptions *option = [[PHImageRequestOptions alloc] init];
    //    option.version = PHImageRequestOptionsVersionOriginal;
    //    option.resizeMode = PHImageRequestOptionsResizeModeNone;
    [[PHImageManager defaultManager] requestImageDataAndOrientationForAsset:self.asset options:nil resultHandler:^(NSData * _Nullable imageData, NSString * _Nullable dataUTI, CGImagePropertyOrientation orientation, NSDictionary * _Nullable info) {
            UIImage *image = [UIImage imageWithData:imageData];
            [self.mediaView.imageView setImage:image];
    }];
}



@end
