//
//  STYLivePhotoViewController.m
//  STYHomework
//
//  Created by bytedance on 2021/7/14.
//

#import "STYLivePhotoViewController.h"

@interface STYLivePhotoViewController ()
@end

@implementation STYLivePhotoViewController

- (instancetype)initWithAsset:(PHAsset *)asset
{
    if (self = [super init]) {
        _asset = asset;
        self.view.frame = [UIScreen mainScreen].bounds;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    PHLivePhotoRequestOptions *option = [[PHLivePhotoRequestOptions alloc] init];
    [[PHImageManager defaultManager] requestLivePhotoForAsset:_asset targetSize:self.view.frame.size contentMode:PHImageContentModeAspectFit options:option resultHandler:^(PHLivePhoto * _Nullable livePhoto, NSDictionary * _Nullable info) {
        PHLivePhotoView *photoView = [[PHLivePhotoView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        [self.view addSubview:photoView];
        photoView.livePhoto = livePhoto;
        [photoView startPlaybackWithStyle:PHLivePhotoViewPlaybackStyleFull];
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
