//
//  STYAlbumCollectionViewController.m
//  STYHomework
//
//  Created by bytedance on 2021/7/13.
//

#import "STYAlbumCollectionViewController.h"
#import "STYPhotoStore.h"
#import "STYPhoto.h"
#import <Photos/Photos.h>
#import "STYMediaViewController.h"
#import "STYVideoPlayerViewController.h"
#import "STYLivePhotoViewController.h"
#import "STYCaptureViewController.h"

@interface STYAlbumCollectionViewController ()
@end

@implementation STYAlbumCollectionViewController

static NSString * const reuseIdentifier = @"Cell";
#pragma mark - Initialization
- (instancetype)init
{
    UICollectionViewLayout *layout = [[UICollectionViewLayout alloc] init];
    self = [super initWithCollectionViewLayout:layout];
    self.view.frame = [UIScreen mainScreen].bounds;
    _assets = [[NSMutableArray alloc] init];
    return self;
}

#pragma mark - View Life Circle
- (void)viewDidLoad {
    [super viewDidLoad];
    // 生成CollectionView对象，并设置单元格属性
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.minimumInteritemSpacing = 0;
    flowLayout.minimumLineSpacing = 0;
    flowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    flowLayout.itemSize = CGSizeMake(130, 173.3);

    self.collectionView = [[UICollectionView alloc] initWithFrame:self.view.frame collectionViewLayout:flowLayout];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.collectionView];
    
    // Register cell classes
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    NSInteger cnt = [_assets count];
    return cnt;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[UICollectionViewCell alloc] init];
    }

    [cell.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    // 获取图片
    NSMutableArray *assets = _assets;
    PHAsset *asset = assets[indexPath.row];
    
    PHImageRequestOptions *option = [[PHImageRequestOptions alloc] init];
    option.resizeMode = PHImageRequestOptionsResizeModeFast;
    __block UIImage *image = nil;
    [[PHImageManager defaultManager] requestImageForAsset:asset
                                                   targetSize:cell.frame.size
                                                  contentMode:PHImageContentModeAspectFit
                                                      options:option
                                                resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
            image = result;
    }];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    imageView.frame = CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height);
    [cell.contentView addSubview:imageView];
    cell.backgroundColor = [UIColor yellowColor];
    return cell;
}


#pragma mark <UICollectionViewDelegate>
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSMutableArray *assets = _assets;
    PHAsset *asset = assets[indexPath.row];
    
    // 如果资源是livePhoto
    if (asset.mediaSubtypes == (PHAssetMediaSubtypePhotoLive | 0x200)) {
        NSLog(@"This is a live Photo.");
        STYLivePhotoViewController *lpvc = [[STYLivePhotoViewController alloc] initWithAsset:asset];
        [self.navigationController pushViewController:lpvc animated:YES];
    }
    else if (asset.mediaType == PHAssetMediaTypeImage) {
        NSLog(@"This is a Image.");
        STYMediaViewController *mediaViewController = [[STYMediaViewController alloc] init];
        mediaViewController.asset = asset;
        [self.navigationController pushViewController:mediaViewController animated:YES];
    }
    else if (asset.mediaType == PHAssetMediaTypeVideo) {
        NSLog(@"This is a Video");
        STYVideoPlayerViewController *playerController = [[STYVideoPlayerViewController alloc] init];
        playerController.asset = asset;
        [self.navigationController pushViewController:playerController animated:YES];
    }
    return YES;
}

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/
    
@end
