//
//  STYMediaViewController.h
//  STYHomework
//
//  Created by bytedance on 2021/7/14.
//

#import <UIKit/UIKit.h>
#import <Photos/Photos.h>

NS_ASSUME_NONNULL_BEGIN

@interface STYMediaViewController : UIViewController
@property (nonatomic, strong) PHAsset *asset;
@end

NS_ASSUME_NONNULL_END
