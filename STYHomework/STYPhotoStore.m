//
//  STYPhotoStore.m
//  STYHomework
//
//  Created by bytedance on 2021/7/13.
//

#import "STYPhotoStore.h"
#import "STYPhoto.h"
#import <Photos/Photos.h>

@interface STYPhotoStore ()
@end

@implementation STYPhotoStore
#pragma mark - Singleton
+ (instancetype)sharedStore
{
    static STYPhotoStore *sharedStore = nil;
    if (sharedStore == nil) {
        sharedStore = [[self alloc] initPrivate];
    }
    return sharedStore;
}

- (instancetype) initPrivate
{
    self = [super init];
    if (self) {
        [self loadAssets];
    }
    return self;
}
// 禁止使用init方法
-(instancetype)init
{
    @throw [NSException exceptionWithName:@"Singleton" reason:@"Use+[STYPhotoStore sharedStore]" userInfo:nil];
    return nil;
}

#pragma mark - Load assets
- (void)loadAssets
{
    // 获取系统创建相册
//    PHFetchResult *smartAlbum = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeAlbumRegular options:nil];
    _allMedias = [[NSMutableArray alloc] init];
    _mediaDict = [[NSMutableDictionary alloc] init];
    _localDict = [[NSMutableDictionary alloc] init];
    _timeDict = [[NSMutableDictionary alloc] init];
    _sortedTimeKey = [[NSMutableArray alloc] init];
    
    PHFetchOptions *fetchOptions = [[PHFetchOptions alloc] init];
    fetchOptions.sortDescriptors = @[
        [NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES],
    ];
    PHFetchResult *fetchResult = [PHAsset fetchAssetsWithOptions:fetchOptions];

    for (PHAsset *asset in fetchResult) {
        [_allMedias addObject:asset];
        // 媒体类型分类
        if (asset.mediaSubtypes == (PHAssetMediaSubtypePhotoLive | 0x200)) {
            if ([[_mediaDict allKeys] containsObject:@"Live Photo"] == YES) {
                [_mediaDict[@"Live Photo"] addObject:asset];
            } else {
                _mediaDict[@"Live Photo"] = [[NSMutableArray alloc] init];
                [_mediaDict[@"Live Photo"] addObject:asset];
            }
        } else if (asset.mediaType == PHAssetMediaTypeImage) {
            if ([[_mediaDict allKeys] containsObject:@"Photo"] == YES) {
                [_mediaDict[@"Photo"] addObject:asset];
            } else {
                _mediaDict[@"Photo"] = [[NSMutableArray alloc] init];
                [_mediaDict[@"Photo"] addObject:asset];
            }
        } else if (asset.mediaType == PHAssetMediaTypeVideo) {
            if ([[_mediaDict allKeys] containsObject:@"Video"] == YES) {
                [_mediaDict[@"Video"] addObject:asset];
            } else {
                _mediaDict[@"Video"] = [[NSMutableArray alloc] init];
                [_mediaDict[@"Video"] addObject:asset];
            }
        }
        
        // 时间分类
        NSDateFormatter *fomatter = [[NSDateFormatter alloc] init];
        [fomatter setDateFormat:@"YYYY-MM-dd"];
        NSString *str = [fomatter stringFromDate:asset.creationDate];
        if ([[_timeDict allKeys] containsObject:str]) {
            [_timeDict[str] addObject:asset];
        } else {
            _timeDict[str] = [[NSMutableArray alloc] init];
            [_timeDict[str] addObject:asset];
        }
        
        
        // 地点分类
    }
    NSArray *tmp = [[_timeDict allKeys] sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        NSComparisonResult res = [obj2 compare:obj1];
        return res;
    }];
    _sortedTimeKey = [tmp mutableCopy];
}


@end
