//
//  STYPhoto.h
//  STYHomework
//
//  Created by bytedance on 2021/7/13.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface STYPhoto : NSObject
@property (nonatomic, strong) UIImage *image;
@end

NS_ASSUME_NONNULL_END
