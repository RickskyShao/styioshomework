//
//  STYAlbumCollectionViewController.h
//  STYHomework
//
//  Created by bytedance on 2021/7/13.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface STYAlbumCollectionViewController : UICollectionViewController <UIToolbarDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>
@property (nonatomic, strong) NSMutableArray *assets;
@end

NS_ASSUME_NONNULL_END
