//
//  STYMediaView.m
//  STYHomework
//
//  Created by bytedance on 2021/7/14.
//

#import "STYMediaView.h"

@interface STYMediaView ()
@end

@implementation STYMediaView

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.imageView = [[UIImageView alloc] init];
        [self setUpImageView];
    }
    [self setNeedsUpdateConstraints];
    return self;
}

- (void)setUpImageView
{
    self.imageView.frame = [UIScreen mainScreen].bounds;
//    [self.imageView setContentScaleFactor:[[UIScreen mainScreen] scale]];
//    self.imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
//    self.imageView.clipsToBounds = YES;
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:self.imageView];
}


@end
