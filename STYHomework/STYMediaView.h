//
//  STYMediaView.h
//  STYHomework
//
//  Created by bytedance on 2021/7/14.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface STYMediaView : UIView
@property (nonatomic) UIImageView *imageView;
@end

NS_ASSUME_NONNULL_END
