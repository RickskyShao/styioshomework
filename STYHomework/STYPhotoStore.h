//
//  STYPhotoStore.h
//  STYHomework
//
//  Created by bytedance on 2021/7/13.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface STYPhotoStore : NSObject
@property (nonatomic, strong) NSMutableArray *allMedias;
@property (nonatomic, strong) NSMutableDictionary *mediaDict;
@property (nonatomic, strong) NSMutableDictionary *localDict;
@property (nonatomic, strong) NSMutableDictionary *timeDict;
@property (nonatomic, strong) NSMutableArray *sortedTimeKey;
+ (instancetype)sharedStore;
- (void)loadAssets;
@end

NS_ASSUME_NONNULL_END
