//
//  STYVideoPlayerView.h
//  STYHomework
//
//  Created by bytedance on 2021/7/14.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <Photos/Photos.h>

NS_ASSUME_NONNULL_BEGIN

@interface STYVideoPlayerView : UIView
@property (nonatomic, strong) AVPlayer *player;
@property (nonatomic, strong) AVPlayerLayer *playerLayer;
@end

NS_ASSUME_NONNULL_END
