//
//  STYRootCollectionViewController.m
//  STYHomework
//
//  Created by bytedance on 2021/7/16.
//

#import "STYRootCollectionViewController.h"
#import "STYPhotoStore.h"
#import "STYAlbumCollectionViewController.h"
#import "STYCaptureViewController.h"
#import <Photos/Photos.h>

@interface STYRootCollectionViewController ()
@property (nonatomic, strong) UISegmentedControl *segControl;
@property (nonatomic, strong) UIButton *cameraButton;
@end

@implementation STYRootCollectionViewController

static NSString * const reuseIdentifier = @"Cell";

# pragma mark - init
- (instancetype)init
{
    UICollectionViewLayout *layout = [[UICollectionViewLayout alloc] init];
    self = [super initWithCollectionViewLayout:layout];
    if (self) {
    }
    return self;
}
# pragma mark - set up UI
- (void)setUpViewUI
{
    self.view.frame = [UIScreen mainScreen].bounds;
    self.view.backgroundColor = [UIColor whiteColor];
    self.collectionView.backgroundColor = [UIColor whiteColor];
}
- (void)setUpCollecCellUI
{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.minimumInteritemSpacing = 10;
    flowLayout.minimumLineSpacing = 40;
    flowLayout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
    flowLayout.itemSize = CGSizeMake(100, 100);
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:self.view.frame collectionViewLayout:flowLayout];
    self.collectionView.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:self.collectionView];
}
- (void)setUpSegControlUI
{
//    NSArray *arr = [[NSArray alloc] initWithObjects:@"媒体类型", @"时间", @"地点", nil];
    NSArray *arr = [[NSArray alloc] initWithObjects:@"媒体类型", @"时间", nil];
    _segControl = [[UISegmentedControl alloc] initWithItems:arr];
    _segControl.frame = CGRectMake(75, 750, 250, 30);
    _segControl.backgroundColor = [UIColor clearColor];
    _segControl.selectedSegmentIndex = 0;
    self.navigationItem.title = @"媒体类型";
    [_segControl addTarget:self action:@selector(controlChanged) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:_segControl];
}

- (void)controlChanged
{
    if (_segControl.selectedSegmentIndex == 0) {
        self.navigationItem.title = @"媒体类型";
    } else if (_segControl.selectedSegmentIndex == 1) {
        self.navigationItem.title = @"时间";
    }
    [self.collectionView reloadData];
}

- (void)setUpButton
{
    CGRect frame = [UIScreen mainScreen].bounds;
    _cameraButton = [[UIButton alloc] initWithFrame:CGRectMake(frame.size.width / 2 - 40, frame.size.height - 210, 80, 80)];
    [_cameraButton addTarget:self action:@selector(takePicture) forControlEvents:UIControlEventTouchDown];
    UIImage *image = [UIImage systemImageNamed:@"camera.circle"];
    [_cameraButton setBackgroundImage:image forState:UIControlStateNormal];
    _cameraButton.tintColor = [UIColor blackColor];
    [self.view addSubview:_cameraButton];
}
# pragma mark - view life circle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpCollecCellUI];
    [self setUpViewUI];
    [self setUpSegControlUI];
    [self setUpButton];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    // Register cell classes
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.collectionView reloadData];
}

#pragma mark <UICollectionViewDataSource>
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSInteger cnt = 0;
    if (_segControl.selectedSegmentIndex == 0) {
        cnt = 3;
    } else if (_segControl.selectedSegmentIndex == 1) {
        cnt = [[STYPhotoStore sharedStore].timeDict allKeys].count;
    }
//    else if (_segControl.selectedSegmentIndex == 2) {
//        return 0;
//    }
    return cnt;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[UICollectionViewCell alloc] init];
    }
    [cell.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    NSString *key = nil;
    NSArray *keyArr = nil;
    if (_segControl.selectedSegmentIndex == 0) {
        keyArr = [[STYPhotoStore sharedStore].mediaDict allKeys];
    } else if (_segControl.selectedSegmentIndex == 1) {
        keyArr = [[STYPhotoStore sharedStore].sortedTimeKey copy];
    }
//    else if (_segControl.selectedSegmentIndex == 2) {
//        key = [[[STYPhotoStore sharedStore].localDict allKeys] objectAtIndex:indexPath.row];
//    }
    key = [keyArr objectAtIndex:indexPath.row];
    
    UIImage *image = [UIImage systemImageNamed:@"folder"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    CGRect frame = cell.contentView.frame;
    imageView.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height - 15);
    [cell.contentView addSubview:imageView];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(frame.origin.x, frame.size.height - 15, frame.size.width, 15)];
    label.text = key;
    label.textAlignment = NSTextAlignmentCenter;
    [cell.contentView addSubview:label];
    
    return cell;
}

#pragma mark <UICollectionViewDelegate>

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/


// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    STYAlbumCollectionViewController *acvc = [[STYAlbumCollectionViewController alloc] init];
    NSString *key = nil;
    if (_segControl.selectedSegmentIndex == 0) {
        key = [[[STYPhotoStore sharedStore].mediaDict allKeys] objectAtIndex:indexPath.row];
        NSMutableArray *sortedArray = [[[[STYPhotoStore sharedStore].mediaDict objectForKey:key] sortedArrayUsingComparator:^NSComparisonResult(PHAsset *obj1, PHAsset *obj2) {
            NSComparisonResult res = [obj2.creationDate compare: obj1.creationDate];
            return res;
        }] mutableCopy];
        acvc.assets = sortedArray;

    } else if (_segControl.selectedSegmentIndex == 1) {
        key = [[STYPhotoStore sharedStore].sortedTimeKey objectAtIndex:indexPath.row];
        NSMutableArray *sortedArray = [[[[STYPhotoStore sharedStore].timeDict objectForKey:key] sortedArrayUsingComparator:^NSComparisonResult(PHAsset *obj1, PHAsset *obj2) {
            NSComparisonResult res = [obj2.creationDate compare: obj1.creationDate];
            return res;
        }] mutableCopy];
        acvc.assets = sortedArray;
    }
//    else if (_segControl.selectedSegmentIndex == 2) {
//        key = [[[STYPhotoStore sharedStore].localDict allKeys] objectAtIndex:indexPath.row];
//        acvc.assets = [[STYPhotoStore sharedStore].localDict objectForKey:key];
//    }
    
    [self.navigationController pushViewController:acvc animated:YES];
    
    return YES;
}


/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/
- (void)takePicture
{
//    UIImagePickerController *ipc = [[UIImagePickerController alloc] init];
//    NSArray *availableTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
//    ipc.mediaTypes = availableTypes;
//    ipc.sourceType = UIImagePickerControllerSourceTypeCamera;
//    ipc.delegate = self;
//    [self presentViewController:ipc animated:YES completion:nil];
    
    STYCaptureViewController *cvc = [[STYCaptureViewController alloc] init];
    [self.navigationController pushViewController:cvc animated:YES];
}
// 关闭相机后，处理照片
//- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<UIImagePickerControllerInfoKey,id> *)info
//{
//    // 保存照片
//    UIImage *image = info[UIImagePickerControllerOriginalImage];
//    if (image) {
//        [[PHPhotoLibrary sharedPhotoLibrary]performChanges:^{
//            [PHAssetChangeRequest creationRequestForAssetFromImage:image];
//        } completionHandler:^(BOOL success, NSError * _Nullable error) {
//            if (error) {
//                NSLog(@"%@",@"照片保存失败");
//            } else {
//                NSLog(@"%@",@"照片保存成功");
//            }
//        }];
//    }
//    // 保存视频
//    NSURL *mediaURL = info[UIImagePickerControllerMediaURL];
//    if (mediaURL) {
//        if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum([mediaURL path])) {
//            UISaveVideoAtPathToSavedPhotosAlbum([mediaURL path], nil, nil, nil);
//        }
//    }
//    [self dismissViewControllerAnimated:YES completion:nil];
//}
@end
