//
//  STYVideoPlayerView.m
//  STYHomework
//
//  Created by bytedance on 2021/7/14.
//

#import "STYVideoPlayerView.h"

@implementation STYVideoPlayerView

- (instancetype)init
{
    self = [super init];
    if (self) {
        _playerLayer = [[AVPlayerLayer alloc] init];
        self.player = [[AVPlayer alloc] init];
    }
    return self;
}

- (AVPlayer *)player {
    return self.playerLayer.player;
}
 
- (void)setPlayer:(AVPlayer *)player {
    self.playerLayer.player = player;
}
 
// Override UIView method
+ (Class)layerClass {
    return [AVPlayerLayer class];
}
 
- (AVPlayerLayer *)playerLayer {
    return (AVPlayerLayer *)self.layer;
}

@end
