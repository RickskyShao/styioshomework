//
//  STYLivePhotoViewController.h
//  STYHomework
//
//  Created by bytedance on 2021/7/14.
//

#import <UIKit/UIKit.h>
#import <Photos/Photos.h>
#import <PhotosUI/PhotosUI.h>

NS_ASSUME_NONNULL_BEGIN

@interface STYLivePhotoViewController : UIViewController
@property (nonatomic, strong) PHAsset *asset;
- (instancetype) initWithAsset:(PHAsset *)asset;
@end

NS_ASSUME_NONNULL_END
